# Introduction #

This simple Sinatra app was used at one time as an automation interface to Windows machines within Mogotest.
It is extremely dangerous and should not be run without taking extreme caution in restricting who can access it or otherwise
authorizing requests.  It's so dangerous, we wrestled with the value in open-sourcing it.  At the end of the day, it serves
as an interesting exercise in how to manipulate the Windows registry and the system proxy configuration in particular.

The code is licensed under the Apache License, version 2.0. This project is no longer maintained but is reasonably up-to-date Ruby.

# Configuring #

Modify `startup.bat` to set the HTTP Basic authentication username and password used to authenticate requests.  All authenticated
requests are also authorized.

# Running #

Don't run it.  Seriously, if you're going to run it, take some time to work out an authorization scheme for your app.  By default,
HTTP Basic authentication is used, but without SSL. If you're running on an untrusted network (bad idea), the password can be
sniffed.  Likewise, no ACL system is in place &mdash; request access is all or nothing.

If you must run, make sure you follow the steps in the "Configuration" section first.  If you haven't already, make sure your checkout
of the project is located at `C:\windows_utility_server` and then run `startup.bat`.  A side benefit of the startup script
approach is it checks for the presence of an `app.rb.new` and will replace `app.rb` if the file exists.  Since Windows does not allow
file handles to be overwritten while open, this simple technique allows you to stage an upgrade that will complete when the process
restarts (e.g., during a system reboot).