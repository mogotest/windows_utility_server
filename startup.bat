REM Check if we have a newer version of the utility server to run.

if exist "C:\windows_utility_server\app.rb.new" move /Y "C:\windows_utility_server\app.rb" "C:\windows_utility_server\app.rb.bak"
if exist "C:\windows_utility_server\app.rb.new" move /Y "C:\windows_utility_server\app.rb.new" "C:\windows_utility_server\app.rb"

REM Install any new or updated dependencies if needed.  We need to spawn in sub-shell via "cmd /c" because bundler will
REM report an error status, even if everything installs just fine.  This would normally cause the remainder of the script
REM to be skipped. By moving to a sub-shell we can move on and successfully boot the server.

cmd /c bundle install

REM Start the Windows Utility Server

SET WINDOWS_UTILITY_USERNAME=mogotest
SET WINDOWS_UTILITY_PASSWORD=change_me

bundle exec torqbox -b 0.0.0.0 -p 9292
