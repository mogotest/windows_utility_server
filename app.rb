require 'rubygems'
require 'bundler/setup'
require 'sinatra/base'
require 'cgi'
require 'fileutils'

class App < Sinatra::Base
  REGISTRY_MUTEX = Mutex.new

  configure :production, :development do
    enable :logging
  end

  use Rack::Auth::Basic, 'Mogotest Realm' do |username, password|
    ! ENV['WINDOWS_UTILITY_USERNAME'].nil? && (username == ENV['WINDOWS_UTILITY_USERNAME']) &&
    ! ENV['WINDOWS_UTILITY_PASSWORD'].nil? && (password == ENV['WINDOWS_UTILITY_PASSWORD'])
  end

  put '/directories' do
    FileUtils.mkdir_p(params[:path])
    200
  end

  put '/files' do
    File.open(params[:path], 'wb') { |f| f.write(params[:file][:tempfile].read) }
    200
  end

  get '/files' do
    begin
      file_listing = Dir.glob(File.join(params[:path], '*'))
      file_listing.join("\n")
    rescue Errno::ENOENT
      404
    end
  end

  get '/file' do
    begin
      File.open(params[:path], 'rb') { |f| f.read }
    rescue Errno::ENOENT
      404
    end
  end

  delete '/file' do
    begin
      FileUtils.rm_r(params[:path])
      200
    rescue Errno::ENOENT
      404
    end
  end

  get '/proxy' do
    REGISTRY_MUTEX.synchronize do
      require "win32/registry"

      key = Win32::Registry::HKEY_CURRENT_USER.open('Software\Microsoft\Windows\CurrentVersion\Internet Settings')

      if key['ProxyEnable'] == 1
        key['ProxyServer']
      else
        404
      end
    end
  end

  delete '/proxy' do
    REGISTRY_MUTEX.synchronize do
      require "win32/registry"

      key = Win32::Registry::HKEY_CURRENT_USER.open('Software\Microsoft\Windows\CurrentVersion\Internet Settings', Win32::Registry::KEY_WRITE)
      key['ProxyEnable'] = 0

      begin
        key.delete('AutoConfigURL')
      rescue
        # This will raise if the registry value doesn't exist.   Since reading if it doesn't exist will also raise, I'm not sure how to
        # check for the value's presence before attempting to delete it.
      end

      200
    end
  end

  post '/proxy' do
    REGISTRY_MUTEX.synchronize do
      require "win32/registry"

      key = Win32::Registry::HKEY_CURRENT_USER.open('Software\Microsoft\Windows\CurrentVersion\Internet Settings', Win32::Registry::KEY_WRITE)

      begin
        key.delete('AutoConfigURL')
      rescue
        # This will raise if the registry value doesn't exist.   Since reading if it doesn't exist will also raise, I'm not sure how to
        # check for the value's presence before attempting to delete it.
      end

      key['ProxyServer'] = "#{params[:host]}:#{params[:port]}"
      key['ProxyOverride'] = '<local>'
      key['MigrateProxy'] = 1
      key['ProxyEnable'] = 1

      201
    end
  end

  put '/exec' do
    begin
      `#{params[:command]}`
    rescue => e
      [400, e.backtrace.to_s]
    end
  end

  put '/eval' do
    begin
      "#{eval(params[:script])}"
    rescue => e
      [400, e.backtrace.to_s]
    end
  end

  private

  def filename
    File.join('C:', 'temp', params[:filename])
  end
end
